var nextPage = function(e){
	e.preventDefault();
	window.location = "registro.html";
}

var nextPage2 = function(e){
	e.preventDefault();
	var nombreJugador1 = document.getElementById("nombre-jugador1").value;
	nombreJugador1=nombreJugador1.charAt(0).toLocaleUpperCase()+nombreJugador1.substring(1,nombreJugador1.length);
	var nombreJugador2 = document.getElementById("nombre-jugador2").value;
	nombreJugador2=nombreJugador2.charAt(0).toLocaleUpperCase()+nombreJugador2.substring(1,nombreJugador2.length);
	if(nombreJugador1.length>0 && nombreJugador2.length>0){
		window.location="juego.html";
		localStorage.setItem("nombreJugador1", nombreJugador1);
		localStorage.setItem("nombreJugador2", nombreJugador2);
	}else{
		alert("Ingresa tu nombre");
	}
}

var count=0;
var countJugador1=0;
var countJugador2=0;

var ganador = $("<p></p>");
ganador.addClass("ganador");
var contenedor = $(".container-info");

var buttonHistorial=$("<button></button>");
var aButton = $("<a></a>");
aButton.append(buttonHistorial);
buttonHistorial.addClass("btn btn-default boton-juego");
buttonHistorial.text("Mostrar historial");
buttonHistorial.attr("id","boton-historial");
aButton.attr("href","historial.html");

var obtenGanador = function(){
	if(($("#uno").hasClass("circulo") && $("#dos").hasClass("circulo") && $("#tres").hasClass("circulo")) ||
	   ($("#tres").hasClass("circulo") && $("#seis").hasClass("circulo") && $("#nueve").hasClass("circulo")) ||
	   ($("#siete").hasClass("circulo") && $("#ocho").hasClass("circulo") && $("#nueve").hasClass("circulo")) ||
	   ($("#uno").hasClass("circulo") && $("#cuatro").hasClass("circulo") && $("#siete").hasClass("circulo")) ||
	   ($("#dos").hasClass("circulo") && $("#cinco").hasClass("circulo") && $("#ocho").hasClass("circulo")) ||
	   ($("#uno").hasClass("circulo") && $("#cinco").hasClass("circulo") && $("#nueve").hasClass("circulo")) ||
	   ($("#siete").hasClass("circulo") && $("#cinco").hasClass("circulo") && $("#tres").hasClass("circulo"))){
		localStorage.setItem("movimientosJugador1",countJugador1);
		localStorage.setItem("movimientosJugador2",countJugador2);
		ganador.text("Ganó "+ localStorage.getItem("nombreJugador1"));
		contenedor.append(ganador);
		contenedor.append(aButton);
		guardarDatos();
	}
	else if(($("#uno").hasClass("equis") && $("#dos").hasClass("equis") && $("#tres").hasClass("equis")) ||
			($("#tres").hasClass("equis") && $("#seis").hasClass("equis") && $("#nueve").hasClass("equis")) ||
			($("#siete").hasClass("equis") && $("#ocho").hasClass("equis") && $("#nueve").hasClass("equis")) ||
			($("#uno").hasClass("equis") && $("#cuatro").hasClass("equis") && $("#siete").hasClass("equis")) ||
			($("#dos").hasClass("equis") && $("#cinco").hasClass("equis") && $("#ocho").hasClass("equis")) ||
			($("#uno").hasClass("equis") && $("#cinco").hasClass("equis") && $("#nueve").hasClass("equis")) ||
			($("#siete").hasClass("equis") && $("#cinco").hasClass("equis") && $("#tres").hasClass("equis"))){
		localStorage.setItem("movimientosJugador1",countJugador1);
		localStorage.setItem("movimientosJugador2",countJugador2);
		ganador.text("Ganó "+ localStorage.getItem("nombreJugador2"));
		contenedor.append(ganador);
		contenedor.append(aButton);
		guardarDatos();

	}
}

function guardarDatos(){
	var request = new XMLHttpRequest();

	request.open('POST', 'http://test-ta.herokuapp.com/games');

	request.setRequestHeader('Content-Type', 'application/json');

	request.onreadystatechange = function () {
		if (this.readyState === 4) {
			console.log('Status:', this.status);
			console.log('Headers:', this.getAllResponseHeaders());
			console.log('Body:', this.responseText);
		}
	};

	var body = {
		'game': {
			'winner_player': localStorage.getItem("nombreJugador1"),
			'loser_player': localStorage.getItem("nombreJugador2"),
			'number_of_turns_to_win': count
		}
	};

	request.send(JSON.stringify(body));
}

var jugar = function(){
	if($(this).hasClass("circulo")|| $(this).hasClass("equis")){}
	else{
		if(count%2==0){
			$("#turno").text(localStorage.getItem("nombreJugador2"));
			$(this).addClass("circulo");
			$(this).html("<span class='glyphicon glyphicon-remove'></span>");
			$("#turno").next().html("<span class='glyphicon glyphicon-heart'></span>");
			countJugador1++;
			$("#contador1").text(countJugador1);
			count++;
			if(count>3){
				obtenGanador();
			}
		}
		else{
			$("#turno").text(localStorage.getItem("nombreJugador1"));
			$(this).addClass("equis");
			$(this).html("<span class='glyphicon glyphicon-heart'></span>");
			$("#turno").next().html("<span class='glyphicon glyphicon-remove'></span>");
			countJugador2++;
			$("#contador2").text(countJugador2);
			count++;
			if(count>3){
				obtenGanador();
			}
		}
		if(count==9){
			ganador.text("Ninguno ganó");
			ganador.addClass("ganador");
			contenedor.append(ganador);
		}
	}
}

var load = function(){
	$("#nombre-jugador1").focus();
	$(".opcion").click(jugar);
	$("#button-iniciar").click(nextPage);
	$("#boton-iniciar-juego").click(nextPage2);
	$("#turno").text(localStorage.getItem("nombreJugador1"));
	$("#turno").next().html("<span class='glyphicon glyphicon-remove'></span>");
	$("#nombre1").text(localStorage.getItem("nombreJugador1"));
	$("#nombre2").text(localStorage.getItem("nombreJugador2"));
	$("#contador1").text("0");
	$("#contador2").text("0");
}

var containerJuegos = $(".container-juegos");

var loadHistorial = function(){
	var plantilla = "<div class='text-right div-historial'>" +
		"<p class='text-center'>__ganador__ le ganó a __perdedor__  en __movimientos__ movimientos</p>" + 
		"<a href='comentar.html'>Comentar</a>" +
		"</div>";
	var juegos = "";
	var request = new XMLHttpRequest();
	request.open('GET', 'http://test-ta.herokuapp.com/games');
	request.onreadystatechange = function () {
		if (this.readyState === 4) {
			console.log('Status:', this.status);
			console.log('Headers:', this.getAllResponseHeaders());
			console.log('Body:', this.responseText);
			var jugadas = JSON.parse(this.responseText);
			$(jugadas).each(function(index, elem){
				juegos += plantilla.replace("__ganador__", elem.winner_player)
					.replace("__perdedor__", elem.loser_player)
					.replace("__movimientos__", elem.number_of_turns_to_win)
			});
			containerJuegos.append(juegos);
		}
	};
	request.send();
}

$("historial.html").ready(loadHistorial);
$(document).ready(load);